﻿using Microsoft.AspNetCore.Blazor;
using Microsoft.AspNetCore.Blazor.Components;
using VCV.Web.Models;
using VCV.Web.Services;

namespace VCV.Web.Pages
{
    public class IndexComponent : BlazorComponent
    {
        private bool isResizing;
        private WindowState draggedWindow;
        private long mouseX;
        private long mouseY;

        protected bool showStartMenu;

        [Inject]
        public ApplicationService ApplicationService { get; set; }

        protected override void OnInit()
        {
            ApplicationService.ApplicationLaunched = () =>
            {
                showStartMenu = false;
                StateHasChanged();
            };

            ApplicationService.ApplicationActivated = StateHasChanged;
        }

        protected void OnStartButtonPressed()
        {
            showStartMenu = !showStartMenu;
            StateHasChanged();
        }

        protected void MouseMove(UIMouseEventArgs e)
        {
            if (draggedWindow != null)
            {
                long dx = e.ClientX - mouseX;
                long dy = e.ClientY - mouseY;
                if (isResizing)
                {
                    if (draggedWindow.Width + dx > 200) draggedWindow.Width += dx;
                    if (draggedWindow.Height + dy > 200) draggedWindow.Height += dy;
                }
                else
                {
                    if (draggedWindow.PosX + dx >= 1) draggedWindow.PosX += dx;
                    if (draggedWindow.PosY + dy >= 1) draggedWindow.PosY += dy;
                }
            }

            mouseX = e.ClientX;
            mouseY = e.ClientY;
        }

        protected void MouseUp(UIMouseEventArgs e) => SetDraggedWindow(null, false);

        protected void OnClick() => showStartMenu = false;

        protected void SetDraggedWindow(WindowState window, bool isResizing)
        {
            draggedWindow = window; this.isResizing = isResizing;
            if (window != null) ApplicationService.Activate(window.Id, false);
        }
    }
}
