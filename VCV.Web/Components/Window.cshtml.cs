﻿using Microsoft.AspNetCore.Blazor;
using Microsoft.AspNetCore.Blazor.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VCV.Web.Models;
using VCV.Web.Services;

namespace VCV.Web.Components
{
    public class WindowComponent : BlazorComponent
    {
        [Parameter]
        protected WindowState State { get; set; }
            = new WindowState();

        [Parameter]
        protected Action<bool> Drag { get; set; }

        [Parameter]
        protected Action Close { get; set; }

        [Inject]
        public ApplicationService ApplicationService { get; set; }

        protected bool showHelp;

        protected RenderFragment Content => b =>
        {
            b.OpenComponent(1, State.Type);
            if(State.LaunchingFile != null)
            {
                b.AddAttribute(0, "LaunchingFile", State.LaunchingFile);
            }
            b.CloseComponent();
        };

        protected void OnMouseDown(bool resize)
        {
            if (State.IsMaxixmized) return;
            Drag?.Invoke(resize);
        }

        protected void Minimize()
        {
            State.IsActive = false;
            State.IsMinimized = true;
            StateHasChanged();
        }
    }
}
