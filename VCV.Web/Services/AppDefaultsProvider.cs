﻿using System;
using System.Linq;
using VCV.Web.Apps;
using VCV.Web.Models;

namespace VCV.Web.Services
{
    public class AppDefaultsProvider
    {
        private readonly AppDefaults[] registry = new AppDefaults[] {
            new AppDefaults(typeof(About), "?", canMaximize: false),
            new AppDefaults(typeof(Zalgo), "Ẑ̬", canMaximize: false, canResize: false, width: 400),
            new AppDefaults(typeof(Weather), "⛈", height: 660),
            new AppDefaults(typeof(MarkdownEditor), "⇓", "Markdown Editor", helpText: "Try saving a file with `.md` extension in order to be able to launch the editor from Storage Explorer."),
            new AppDefaults(typeof(StorageExplorer), "🗀", "Storage Explorer"),
            new AppDefaults(typeof(Dice), "⚄", height: 550, canResize: false, canMaximize: false),
            new AppDefaults(typeof(Landmines), "✹", height: 420, width: 420, helpText: "Left click to reveal, right click to flag. To win flag all the mines and reveal all the other fields."),
            new AppDefaults(typeof(CurriculumVitae), "🖺", "Curriculum Vitae")
        };

        public AppDefaults GetForType(Type type) => registry.FirstOrDefault(x => x.Type == type);
    }
}
